package pojo;

//购物车的商品项
public class SportItem implements Comparable{
    private Integer SportsId;
    private String SportsName;
    private Integer UserId;
    private String username;
    private double scors;
    private int rank;

    public SportItem() {
    }

    public SportItem(Integer sportsId, String sportsName, Integer userId, String username, double scors, int rank) {
        SportsId = sportsId;
        SportsName = sportsName;
        UserId = userId;
        this.username = username;
        this.scors = scors;
        this.rank = rank;
    }

    public Integer getSportsId() {
        return SportsId;
    }

    public void setSportsId(Integer sportsId) {
        SportsId = sportsId;
    }

    public String getSportsName() {
        return SportsName;
    }

    public void setSportsName(String sportsName) {
        SportsName = sportsName;
    }

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public double getScors() {
        return scors;
    }

    public void setScors(double scors) {
        this.scors = scors;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "SportItem{" +
                "SportsId=" + SportsId +
                ", SportsName='" + SportsName + '\'' +
                ", UserId=" + UserId +
                ", username=" + username +
                ", scors=" + scors +
                ", rank=" + rank +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        //按照名字从小到大排列
        if(o instanceof SportItem){
            SportItem s = (SportItem) o;
            if(s.scors - this.scors >= 0) {
                return 1;
            }else {
                return -1;
            }
        }else {
            throw new IllegalArgumentException("参数错误");
        }
    }
}
