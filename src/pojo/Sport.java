package pojo;

import java.sql.Date;

public class Sport {
    private Integer id;
    private String name;
    private String ruler;
    private String place;
    private Date time;
    private String imgPath = "static/img/default.jpg";

    public Sport() {
    }

    public Sport(Integer id, String name, String ruler, String place, Date time, String imgPath) {
        this.id = id;
        this.name = name;
        this.ruler = ruler;
        this.place = place;
        this.time = time;
        this.imgPath = imgPath;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRuler() {
        return ruler;
    }

    public void setRuler(String ruler) {
        this.ruler = ruler;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        if(imgPath != null && !"".equals(imgPath)) {
            this.imgPath = imgPath;
        }
    }

    @Override
    public String toString() {
        return "Sport{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ruler='" + ruler + '\'' +
                ", place='" + place + '\'' +
                ", time=" + time +
                ", imgPath='" + imgPath + '\'' +
                '}';
    }
}
