package service.impl;

import dao.ItemDao;
import dao.impl.ItemDaoImpl;
import pojo.SportItem;
import service.ItemService;

import java.util.List;

public class ItemServiceImpl implements ItemService{

    ItemDao itemDao = new ItemDaoImpl();

    @Override
    public int addItem(SportItem sportItem) {
        return itemDao.addItem(sportItem);
    }

    @Override
    public List<SportItem> queryItemsBySportsId(Integer id) {
        return itemDao.queryItemsBySportsId(id);
    }

    @Override
    public List<SportItem> queryItemsByUserId(Integer id) {
        return itemDao.queryItemsByUserId(id);
    }

    @Override
    public int updateItem(SportItem sportItem) {
        return itemDao.updateItem(sportItem);
    }

    @Override
    public SportItem querysportItem(Integer sportsId, Integer UserId) {
        return itemDao.querysportItem(sportsId,UserId);
    }
}
