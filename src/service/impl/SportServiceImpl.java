package service.impl;

import dao.SportDao;
import dao.impl.SportDaoImpl;
import pojo.Page;
import pojo.Sport;
import service.SportService;

import java.util.List;

public class SportServiceImpl implements SportService {

    private SportDao SportDao = new SportDaoImpl();

    @Override
    public void addsport(Sport Sport) {
        SportDao.addsport(Sport);
    }

    @Override
    public void deletesportById(Integer id) {
        SportDao.deletesportById(id);
    }

    @Override
    public void updatesport(Sport Sport) {
        SportDao.updatesport(Sport);
    }

    @Override
    public Sport querysportById(Integer id) {
        return SportDao.querysportById(id);
    }

    @Override
    public List<Sport> querysports() {
        return SportDao.querysports();
    }

    @Override
    public Page<Sport> page(int pageNo, int pageSize) {
        Page<Sport> page = new Page<Sport>();


        //设置每页显示的数量
        page.setPageSize(pageSize);

        //求总记录数
        Integer pageTotalCount = SportDao.queryForPageTotalCount();
        //设置总的记录数
        page.setPageTotalCount(pageTotalCount);

        //求总页码
        Integer pageTotal = pageTotalCount / pageSize;
        if(pageTotalCount % pageSize > 0){
            pageTotal++;
        }
        //设置总页码
        page.setPageTotal(pageTotal);

        //设置当前页码
        page.setPageNo(pageNo);

        //求当前页数据的开始索引
        int begin = (page.getPageNo() - 1) * pageSize;
        //求当前页数据
        List<Sport> items = SportDao.queryForPageItems(begin,pageSize);
        //设置当前页数据
        page.setItems(items);

        return page;
    }


}

