package service;

import pojo.SportItem;

import java.util.List;

public interface ItemService {
    int addItem(SportItem sportItem);

    List<SportItem> queryItemsBySportsId(Integer id);

    List<SportItem> queryItemsByUserId(Integer id);

    int updateItem(SportItem sportItem);

    SportItem querysportItem(Integer sportsId, Integer UserId);
}
