package service;

import pojo.Page;
import pojo.Sport;

import java.util.List;

public interface SportService {

    public void addsport(Sport Sport);

    public void deletesportById(Integer id);

    public void updatesport(Sport Sport);

    public Sport querysportById(Integer id);

    public List<Sport> querysports();

    Page<Sport> page(int pageNo, int pageSize);

}
