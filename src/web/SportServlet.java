package web;

import pojo.Page;
import pojo.Sport;
import service.SportService;
import service.impl.SportServiceImpl;
import utils.webUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SportServlet extends BaseServlet {

    SportService sportService = new SportServiceImpl();

//    @Override
////    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
////
////    }
    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //得到餐品添加时的页码
        int pageNo = webUtils.parseInt(req.getParameter("pageNo"),0);
        //每次让页码加一，保证能够重定向到现在餐品所在的页码
        pageNo++;
        //1.获取请求的参数-->封装成为sport对象
        Sport Sport = webUtils.copyParamToBean(req.getParameterMap(), new Sport());
        //2.调用sportService.addsport()保存餐品
        sportService.addsport(Sport);
        //3.跳到餐品列表页面
        //请求重定向
        resp.sendRedirect(req.getContextPath() + "/manager/sportServlet?action=page&pageNo=" + pageNo);
    }
    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //1.获取请求的参数和餐品编号
        int id = webUtils.parseInt(req.getParameter("id"),0);
        //2.调用sportService.delete()删除餐品
        sportService.deletesportById(id);
        //3.重定向回餐品列表管理页面
        resp.sendRedirect(req.getContextPath() + "/manager/sportServlet?action=page&pageNo=" + req.getParameter("pageNo"));
    }
    protected void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //获取请求的参数-->封装成为sport对象
        Sport Sport = webUtils.copyParamToBean(req.getParameterMap(), new Sport());
        //调用sportService.updatesport(sport)修改餐品
        sportService.updatesport(Sport);
        //重定向回餐品列表管理页面
        resp.sendRedirect(req.getContextPath() + "/manager/sportServlet?action=page&pageNo=" + req.getParameter("pageNo"));
    }
    protected void list(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.通过sportServlet查询全部餐品
        List<Sport> sports = sportService.querysports();
        //2.把全部餐品保存到Request域中
        req.setAttribute("sports", sports);
        //3.请求转发到/pages/manager/sport_manager.jsp页面
        req.getRequestDispatcher("/pages/manager/sport_manager.jsp").forward(req, resp);
    }
    protected void getsport(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取请求的参数和餐品编号
        int id = webUtils.parseInt(req.getParameter("id"), 0);
        //2.调用sportService.querysportById查询餐品
        Sport Sport = sportService.querysportById(id);
        //3.保存餐品到request域中
        req.setAttribute("sport", Sport);
        //4.请求转发到/pages/manager/sport_edit.jsp页面
        req.getRequestDispatcher("/pages/manager/sport_edit.jsp").forward(req, resp) ;
    }

    //处理分页功能
    protected void page(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取请求的参数 pageNo和pageSize
        int pageNo = webUtils.parseInt(req.getParameter("pageNo"), 1);
        int pageSize = webUtils.parseInt(req.getParameter("pageSize"), Page.PAGE_SIZE);
        //2.调用sportService.page(pageNo,pageSize)，返回page对象
        Page<Sport> page = sportService.page(pageNo,pageSize);

        //设置url
        page.setUrl("manager/sportServlet?action=page");

        //3.保存Page对象到Request域中
        req.setAttribute("page",page);
        //4.请求转发到pages/manager/sport_manager.jsp中
        req.getRequestDispatcher("/pages/manager/sport_manager.jsp").forward(req, resp);
    }
}
