package web;

import pojo.Page;
import pojo.Sport;
import service.SportService;
import service.impl.SportServiceImpl;
import utils.webUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientSportServlet extends BaseServlet{

    private SportService sportService = new SportServiceImpl();

    //处理分页功能
    protected void page(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取请求的参数 pageNo和pageSize
        int pageNo = webUtils.parseInt(req.getParameter("pageNo"), 1);
        int pageSize = webUtils.parseInt(req.getParameter("pageSize"), Page.PAGE_SIZE);
        //2.调用sportService.page(pageNo,pageSize)，返回page对象
        Page<Sport> page = sportService.page(pageNo,pageSize);

        //设置url
        page.setUrl("client/sportServlet?action=page");

        //3.保存Page对象到Request域中
        req.setAttribute("page",page);
        //4.请求转发到pages/pages/client/index.jsp中
        req.getRequestDispatcher("/pages/client/index.jsp").forward(req, resp);
    }

}
