package utils;

import org.apache.commons.beanutils.BeanUtils;

import java.util.Map;

public class webUtils {
    public static <T> T copyParamToBean(Map value, T bean){
        try {
            BeanUtils.populate(bean,value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }

    //将字符串转换成为int类型的数据
    public static int parseInt(String strInt, int defaultValue){
        try {
            if (strInt == null){
                return defaultValue;
            }
            return Integer.parseInt(strInt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultValue;
    }
}
