package utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;


    public HikariConfig config(){
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("org.hsqldb.jdbc.JDBCDriver");
        config.setJdbcUrl("jdbc:hsqldb:hsql://localhost/");
        config.setUsername("root");
        config.setPassword("704256750");
        config.setMaximumPoolSize(5);
        config.setMinimumIdle(2);
        return config;
    }
    @Bean
    public HikariDataSource dataSource(){
        HikariDataSource ds = new HikariDataSource(config());
        return ds;
    }

