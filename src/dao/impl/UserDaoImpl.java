package dao.impl;

import dao.UserDao;
import pojo.User;

public class UserDaoImpl extends BaseDao implements UserDao {
    @Override
    public User queryUserByUsername(String username) {
        String sql = "select id,username,gender,`password`,status from t_user where username = ?";
        return queryForOne(User.class,sql,username);
    }

    @Override
    public int saveUser(User user) {
        String sql = "insert into t_user(username,gender,password,status)values(?,?,?,?)";
        return update(sql,user.getUsername(),user.getGender(),user.getPassword(),user.getStatus());
    }

    @Override
    public User queryUserByUsernameAndPassword(String username, String password) {
        String sql = "select id,username,gender,`password`,status from t_user where username = ? and password = ?";
        return queryForOne(User.class,sql,username,password);
    }

    @Override
    public User queryUserByUserId(Integer userId) {
        String sql = "select id,username,gender,`password`,status from t_user where id = ?";
        return queryForOne(User.class,sql,userId);
    }
}
