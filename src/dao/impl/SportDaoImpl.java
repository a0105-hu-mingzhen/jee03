package dao.impl;

import dao.SportDao;
import pojo.Sport;

import java.util.List;

public class SportDaoImpl extends BaseDao implements SportDao {
    @Override
    public int addsport(Sport Sport) {
        String sql = "insert into t_sport(`name`,ruler,place,time,img_path)values(?,?,?,?,?)";
        return update(sql, Sport.getName(), Sport.getRuler(), Sport.getPlace(), Sport.getTime(),Sport.getImgPath());
    }

    @Override
    public int deletesportById(Integer id) {
        String sql = "delete from t_sport where id = ?";
        return update(sql,id);
    }

    @Override
    public int updatesport(Sport Sport) {
        String sql = "update t_sport set `name`=?,ruler=?,place=?,time=?,img_path=? where id=?";
        return update(sql, Sport.getName(), Sport.getRuler(), Sport.getPlace(), Sport.getTime(), Sport.getImgPath(), Sport.getId());
    }

    @Override
    public Sport querysportById(Integer id) {
        String sql = "select id,`name`,ruler,place,time,img_path imgPath from t_sport where id=?";
        return queryForOne(Sport.class,sql,id);
    }

    @Override
    public List<Sport> querysports() {
        String sql = "select id,`name`,ruler,place,time,img_path imgPath from t_sport";
        return queryForList(Sport.class,sql);
    }

    @Override
    public Integer queryForPageTotalCount() {
        String sql = "select count(*) from t_sport";
        Number count = (Number) queryForSingleValue(sql);
        return count.intValue();
    }

    @Override
    public List<Sport> queryForPageItems(int begin, int pageSize) {
        String sql = "select id,`name`,ruler,place,time,img_path imgPath from t_sport limit ?,?";
        return queryForList(Sport.class,sql,begin,pageSize);
    }

}
