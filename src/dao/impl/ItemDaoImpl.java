package dao.impl;

import dao.ItemDao;
import pojo.SportItem;

import java.util.List;

public class ItemDaoImpl extends BaseDao implements ItemDao {
    @Override
    public int addItem(SportItem sportItem) {
        String sql = "insert into t_sp_par(sports_id,sports_name,user_id,username)values(?,?,?,?)";
        return update(sql,sportItem.getSportsId(),sportItem.getSportsName(),sportItem.getUserId(),sportItem.getUsername());
    }

    @Override
    public List<SportItem> queryItemsBySportsId(Integer id) {
        String sql = "select `sports_id` SportsId, `sports_name` SportsName, user_id UserId, " +
                "username, scores scors, `rank` from t_sp_par where sports_id=?";
        return queryForList(SportItem.class, sql, id);
    }

    @Override
    public List<SportItem> queryItemsByUserId(Integer id) {
        String sql = "select `sports_id` SportsId, `sports_name` SportsName, user_id UserId, " +
                "username, scores scors, `rank` from t_sp_par where user_id=?";
        return queryForList(SportItem.class, sql, id);
    }

    @Override
    public int updateItem(SportItem sportItem) {
        String sql = "update t_sp_par set scores=? where sports_id=? and user_id=?";
        return update(sql,sportItem.getScors(),sportItem.getSportsId(),sportItem.getUserId());
    }

    @Override
    public SportItem querysportItem(Integer sportsId, Integer UserId) {
        String sql = "select `sports_id` SportsId, `sports_name` SportsName, user_id UserId, " +
                "username, scores, `rank` from t_sp_par where sports_id=? and user_id=?";
        return queryForOne(SportItem.class,sql,sportsId,UserId);
    }
}
