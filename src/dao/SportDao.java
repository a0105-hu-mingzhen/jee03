package dao;

import pojo.Sport;

import java.util.List;

public interface SportDao {
    public int addsport(Sport Sport);

    public int deletesportById(Integer id);

    public int updatesport(Sport Sport);

    public Sport querysportById(Integer id);

    public List<Sport> querysports();

    Integer queryForPageTotalCount();

    List<Sport> queryForPageItems(int begin, int pageSize);

}





