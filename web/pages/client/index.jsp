<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>运动会系统首页</title>
	<%--静态包含base标签，css样式，jQuery文件--%>
	<%@include file="/pages/common/head.jsp"%>
	<script type="text/javascript">
		<%--<c:if test="${not empty sessionScope.user && sessionScope.user.status == 1}">--%>
		$(function () {
			//给加入购物车按钮加入单击事件
			$("button.addItem").click(function () {
				var sportsId = $(this).attr("sportsId");
				location.href = "${basePath}itemServlet?action=addItem&id=" + sportsId;
			})
		});
		<%--</c:if>--%>
		<%--<c:if test="${not empty sessionScope.user && (sessionScope.user.status == 2 || sessionScope.user.status == 3)}">--%>
		$(function () {
			//给加入购物车按钮加入单击事件
			$("button.rank").click(function () {
				var sportsId = $(this).attr("sportsId");
				location.href = "${basePath}itemServlet?action=sportsId&sportsid=" + sportsId;
			})
		});
		<%--</c:if>--%>
		var admin = "admin";
	</script>
</head>
<body>

	<div id="header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
			<span class="wel_word">高校考试预约系统</span>
			<div>
				<%--如果用户还没有登录，显示  【登录和注册的菜单】--%>
				<c:if test="${empty sessionScope.user}">
					<a href="pages/user/login.jsp">登录</a> |
					<a href="pages/user/regist.jsp">注册</a>
				</c:if>
				<c:if test="${not empty sessionScope.user && sessionScope.user.username eq 'admin'
				 		&&sessionScope.user.password eq 'admin' }">
					<span>欢迎管理员<span class="um_span">Admin</span>光临高校考试预约系统</span>
					<a href="manager/sportServlet?action=page">考试管理</a>
					<a href="userServlet?action=logout">注销</a>&nbsp;&nbsp;
				</c:if>
				<%--如果用户已经登录，则显示登录成功之后的用户信息--%>
				<c:if test="${not empty sessionScope.user && sessionScope.user.status == 1}">
					<span>欢迎考生<span class="um_span">${sessionScope.user.username}</span>光临高校考试预约系统</span>
					<a href="${basePath}itemServlet?action=userId&userid=${sessionScope.user.id}">报名考试</a>
					<a href="userServlet?action=logout">注销</a>&nbsp;&nbsp;
<%--					<a href="pages/manager/manager.jsp">后台管理</a>--%>
				</c:if>
				<c:if test="${not empty sessionScope.user && sessionScope.user.status == 2}">
					<span>欢迎裁判员<span class="um_span">${sessionScope.user.username}</span>光临运动会管理系统</span>
					<%--<a href="pages/item/item.jsp">公布排名</a>--%>
					<a href="userServlet?action=logout">注销</a>&nbsp;&nbsp;
					<%--					<a href="pages/manager/manager.jsp">后台管理</a>--%>
				</c:if>
				<c:if test="${not empty sessionScope.user && sessionScope.user.status == 3}">
					<span>欢迎老师<span class="um_span">${sessionScope.user.username}</span>光临高校考试预约系统</span>
					<a href="userServlet?action=logout">注销</a>&nbsp;&nbsp;
					<%--					<a href="pages/manager/manager.jsp">后台管理</a>--%>
				</c:if>
			</div>
	</div>
	<div id="main">
		<div id="sport">
			<div>
				<br>
				<br>
			</div>
			<c:forEach items="${requestScope.page.items}" var="sport">
			<div class="b_list">
				<div class="img_div">
					<img class="food_img" alt="" src="${sport.imgPath}" />
				</div>
				<div class="food_info">
					<div class="food_name">
						<span class="sp1">考试名:</span>
						<span class="sp2">${sport.name}</span>
					</div>
					<div class="food_author">
						<span class="sp1">考试规则:</span>
						<span class="sp2">${sport.ruler}</span>
					</div>
					<div class="food_price">
						<span class="sp1">举办地点:</span>
						<span class="sp2">${sport.place}</span>
					</div>
					<div class="food_sales">
						<span class="sp1">举办时间:</span>
						<span class="sp2">${sport.time}</span>
					</div>
					<c:if test="${empty sessionScope.user}"/>
					<c:if test="${sessionScope.user.status == 1}">
					<div class="food_add">
						<button sportsId = "${sport.id}" class="addItem">报名</button>
					</div>
					</c:if>
					<c:if test="${sessionScope.user.status == 2}">
						<div class="food_add">
							<button sportsId = "${sport.id}" class="rank">打分</button>
						</div>
					</c:if>
					<c:if test="${sessionScope.user.status == 3}">
						<div class="food_add">
							<button sportsId = "${sport.id}" class="rank">查看报名</button>
						</div>
					</c:if>
				</div>
			</div>
			</c:forEach>

		</div>
		<%--静态包含分页条--%>
		<%@include file="/pages/common/page_nav.jsp"%>

	</div>
	<%--静态包含页脚内容--%>
	<%@include file="/pages/common/footer.jsp"%>
</body>
</html>