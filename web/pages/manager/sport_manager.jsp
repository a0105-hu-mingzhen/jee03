<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>考试管理</title>
	<%--静态包含base标签，css样式，jQuery文件--%>
	<%@include file="/pages/common/head.jsp"%>
	<script type="text/javascript">
		$(function () {
			$("a.deleteClass").click(function () {
				//在事件的function函数中，有一个this对象，是当前正在响应事件的dom对象。
				/*
				* confirm是确认提示框函数
				* 参数是它的提示内容
				* 它有两个按钮，一个确认，一个取消
				* 返回true,表示点击了确认。返回false，表示点击了取消
				* */
				return confirm("你确定要删除【 " + $(this).parent().parent().find("td:first").text() + "】?");
			})
		})
	</script>
</head>
<body>
	
	<div id="header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
			<span class="wel_word">高校考试预约系统</span>
		<%--静态包含manager管理模块的菜单--%>
		<%@include file="/pages/common/manager_menu.jsp"%>
	</div>
	
	<div id="main">
		<table>
			<tr>
				<td>考试</td>
				<td>规则</td>
				<td>举办地点</td>
				<td>举办时间</td>
				<td colspan="2">操作</td>
			</tr>		
			<c:forEach items="${requestScope.page.items}" var="sport">
				<tr>
					<td>${sport.name}</td>
					<td>${sport.ruler}</td>
					<td>${sport.place}</td>
					<td>${sport.time}</td>
					<td><a href="manager/sportServlet?action=getsport&id=${sport.id}&pageNo=${requestScope.page.pageNo}">修改</a></td>
					<td><a class="deleteClass" href="manager/sportServlet?action=delete&id=${sport.id}&pageNo=${requestScope.page.pageNo}">删除</a></td>
				</tr>
			</c:forEach>
			
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><a href="pages/manager/sport_edit.jsp?pageNo=${requestScope.page.pageTotal}">添加考试</a></td>
			</tr>	
		</table>

		<%--静态包含分页条--%>
		<%@include file="/pages/common/page_nav.jsp"%>

	</div>

	<%--静态包含页脚内容--%>
	<%@include file="/pages/common/footer.jsp"%>
</body>
</html>