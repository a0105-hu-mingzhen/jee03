<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>项目</title>
	<%--静态包含base标签，css样式，jQuery文件--%>
	<%@include file="/pages/common/head.jsp"%>
</head>
<body>
	
	<div id="header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
			<span class="wel_word">考试</span>
		<%--静态包含，登陆成功之后的菜单--%>
		<%@include file="/pages/common/login_success_menu.jsp"%>

	</div>
	
	<div id="main">
	
		<table>
			<tr>
				<td>考试</td>
				<td>选手名</td>
				<td>参与</td>
				<td>序号</td>
				<c:if test="${sessionScope.user.status == 2}">
					<td>确认</td>
				</c:if>
			</tr>


			<c:forEach items="${requestScope.sportItems}" var="item">
			<tr>
				<td>${item.sportsName}</td>

				<td>${item.username}</td>
				<c:if test="${sessionScope.user.status == 3 ||sessionScope.user.status == 1}">
					<td>${item.scors}</td>
					<td>${item.rank}</td>
				</c:if>
				<c:if test="${sessionScope.user.status == 2}">
					<td><input id="scores" type="text" style="width: 80px"
						   value="${item.scors}"></td>
					<td>${item.rank}</td>
					<td><a href="itemServlet?action=update&sportsid=${item.sportsId}&userid=${item.userId}&scores=${88}">打分</a></td>

				</c:if>
			</tr>
			</c:forEach>


		</table>

	</div>

	<%--静态包含页脚内容--%>
	<%@include file="/pages/common/footer.jsp"%>
</body>
</html>